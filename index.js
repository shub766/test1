console.log("****AES256*****")
const crypto = require('crypto');
const NodeRSA = require('node-rsa');

const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
  
function encrypt(text) {
let cipher = crypto.createCipheriv('aes-256-cbc',Buffer.from(key), iv);
let encrypted = cipher.update(text);
encrypted = Buffer.concat([encrypted, cipher.final()]);
return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}
  
function decrypt(text) {
let iv = Buffer.from(text.iv, 'hex');
let encryptedText = Buffer.from(text.encryptedData, 'hex');
let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
let decrypted = decipher.update(encryptedText);
decrypted = Buffer.concat([decrypted, decipher.final()]);
return decrypted.toString();
}
  
var gfg = encrypt(":)");
console.log(gfg);

var decrptedvalue = decrypt(gfg);
console.log(decrptedvalue)

// const alice = crypto.createECDH('secp256k1');
// alice.generateKeys();

// const bob = crypto.createECDH('secp256k1');
// bob.generateKeys();

// const alicepublickeybase64 = alice.getPublicKey().toString('base64')
// const bobpublickeybase64 = bob.getPublicKey().toString('base64')

// const alicesharedkey = alice.computeSecret(bobpublickeybase64,'base64','hex')
// const bobsharedkey = bob.computeSecret(alicepublickeybase64,'base64','hex')

// const aes256 = require('aes256')

// const message = ':)';

// const encryptedvalue =  aes256.encrypt(alicesharedkey,message);
// console.log(encryptedvalue)


// const decryptedvalue =  aes256.decrypt(bobsharedkey,encryptedvalue);
// console.log(decryptedvalue)

console.log('*********sha512*******')

let hash = crypto
    .createHash('sha512')
    .update('your message shubham singh')
    .digest('hex');

console.log(hash);


console.log('******RSA*******')

const nodersa = require('node-rsa');

// const keyone = new nodersa({b:1024})

let secret = "This is a non readable secret";

// var encryptedstring =  keyone.encrypt(secret,'base64');

// console.log(encryptedstring);

// var decryptedstring = keyone.decrypt(encryptedstring,'utf-8')

// console.log(decryptedstring);

// var public_key = keyone.exportKey('public');
// var private_key = keyone.exportKey('private');

// console.log(public_key + '\n' + private_key)

public_key = '-----BEGIN PUBLIC KEY-----\n' +
'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDu7R811W8kVhHjq6nJPQuRnlyt\n' +
'unZAvzD8r7Zrqhqww9YJg2cQ5fIjkTBBtj2IcsFnU1UKviOakQQs+9AGyV3sNEYT\n' +
'gwawPjYVCegq+61pCEraYITZ1hDuLI/tMISHH7JDdqR1/Oqy/zKr34orDwArtJCT\n' +
'J5o09hBEhMJTieAwXwIDAQAB\n' +
'-----END PUBLIC KEY-----\n';

private_key = '-----BEGIN RSA PRIVATE KEY-----\n' +
'MIICXQIBAAKBgQDu7R811W8kVhHjq6nJPQuRnlytunZAvzD8r7Zrqhqww9YJg2cQ\n' +
'5fIjkTBBtj2IcsFnU1UKviOakQQs+9AGyV3sNEYTgwawPjYVCegq+61pCEraYITZ\n' +
'1hDuLI/tMISHH7JDdqR1/Oqy/zKr34orDwArtJCTJ5o09hBEhMJTieAwXwIDAQAB\n' +
'AoGAV94wyjWSSES3E4nASVf/9y/MWb/nWWx28ZcVeYb3heafwqPmtZo7cMUmgCQr\n' +
'gRDi+8Lz1Z2TrnaCmKCLItPPjCa+G1xtqwZW46EMjFYoO8nxMkmT4lyCgWQEPHG0\n' +
'/a4Rd1OJ6mjVxpeS+/495uRHPz/6varUgj87XIXaZOy6O/ECQQD9jOxyPWKl+QAv\n' +
'rpakcmBgzlxffhMUBoouSzgHvinzN3AuxTiLsk5AzWo6zyAjg4R7YVqYlsqw5ZyE\n' +
'b1jRC+zVAkEA8TwHqaPxQEWkioTkgSiIHFV3XjJYtvwop+XqkJ3sd9aESdWeEaVV\n' +
'f4VGOA1j0fOIylEry88f4TjkRrFPvvoyYwJBAIaTovhrzo1roYatczWro64eOfdR\n' +
'cozhf7xcnku1yoxWc4vPkFjNdi19GhJG17ch2lKmdLCi8Mh9xtqgQxMJ8lUCQQDh\n' +
'4Li4EkcSTGOORHMFsSMJjMlRjUjeRev3kmP5d7X0XAvQatIwaaRN3EfssjT2YNeD\n' +
'wU+F7fJG3BN7Ugl+pB1LAkBtw+CJ9l3UMbqxWwfvzjyIcQ0OTuezJTVgyDhjliU8\n' +
'4eNKHoX0KvGv4BjOpgYoMQ3YvwjlHzxOoVC6Vi/gkQqM\n' +
'-----END RSA PRIVATE KEY-----\n';

let key_private = new NodeRSA(private_key);
let key_public = new NodeRSA(public_key);

//public key for encryption

var encryptedstring = key_public.encrypt(secret,'base64');
console.log(encryptedstring);

//private key for decryption

var decryptedstring = key_private.decrypt(encryptedstring,'utf-8');
console.log(decryptedstring);